import React from 'react';
import logo from './logo.svg';
import './App.css';
import externalConfig from 'externalConfig'

function App() {
  return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            This is your external configuration key:
            <br /> {externalConfig.exampleKey}
          </p>
        </header>
      </div>
  );
}

export default App;